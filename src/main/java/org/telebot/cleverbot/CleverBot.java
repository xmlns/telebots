package org.telebot.cleverbot;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotSession;
import com.google.code.chatterbotapi.ChatterBotType;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.api.methods.groupadministration.LeaveChat;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 9/10/16.
 */
public class CleverBot extends TelegramLongPollingBot {
    public static final String SUCKS_CHAT_ID = "-1001033891017";

    private final Map<String, ChatterBotSession> sessions = new HashMap<>();

    @Override
    public void onUpdateReceived(final Update update) {
        if (update.hasMessage()) {
            final Message message = update.getMessage();
            final int timestamp = message.getDate();
            final String chat_id = message.getChatId().toString();

//            System.out.println("chatId=" + chat_id);
//            sendMsg("test", SUCKS_CHAT_ID, -1);

            //Don't respond to PMs, non-searchable groups
            if (!message.isSuperGroupMessage()) {
                return;
            }

            //Leave disallowed groups
            if (!SUCKS_CHAT_ID.equals(chat_id)) {
                leave(chat_id);
                return;
            }

            //Respond only to recent messages
            if (System.currentTimeMillis() / 1000 - timestamp > 3) return;

            final String body = message.getText();
            if (message.isCommand()) {
                if (body.toLowerCase().startsWith("/cb ") || body.toLowerCase().startsWith("/cb@cleverrbot ")) {
                    respond(message, chat_id, body.split(" ", 2)[1]);
                }
            } else if (message.isReply() && message.hasText()) {
                if (getBotUsername().equalsIgnoreCase(message.getReplyToMessage().getFrom().getUserName())) {
                    respond(message, chat_id, body);
                }
            }
        }
    }

    private void respond(Message message, String chat_id, String body) {
        System.out.println("CB query: " + body);
        final String response = getResponse(chat_id, body);
        sendMsg(response, chat_id, message.getMessageId());
    }

    private String getResponse(String chat_id, String body) {
        ChatterBotSession session = getCleverBot(chat_id);
        try {
            return session.think(body);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }
    }

    private ChatterBotSession getCleverBot(String chat_id) {
        ChatterBotSession session = sessions.get(chat_id);
        if (session == null) {
            ChatterBotFactory factory = new ChatterBotFactory();

            try {
                ChatterBot bot = factory.create(ChatterBotType.CLEVERBOT);
                session = bot.createSession();
                sessions.put(chat_id, session);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return session;
    }

    public void sendMsg(String msg, String chatId, Integer replyId) {
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.setText(msg);
        sendMessageRequest.setChatId(chatId);
        if (replyId != -1) {
            sendMessageRequest.setReplyToMessageId(replyId);
        }
        try {
            sendMessage(sendMessageRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void leave(final String chat_id) {
        LeaveChat leave = new LeaveChat();
        leave.setChatId(chat_id);
        try {
            leaveChat(leave);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "CleverrBot";
    }

    @Override
    public String getBotToken() {
        return "293517888:AAEi1B15W3i81mDMaMl7KcxmdvhWc7Fsz4w";
    }


}
