package org.telebot;

import org.telebot.updateshandlers.Bot;
import org.telebot.updateshandlers.InlineBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.logging.BotLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ruben Bermudez
 * @version 1.0
 * @brief Main class to create all bots
 * @date 20 of June of 2015
 */

public class Main {
    final private static String LOGTAG = "RINN_BOT";

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            //RinnsBot
            final List<String> brinn_admins = new ArrayList<>(Arrays.asList("Rejected", "xmlns", "BitwisePi"));
            telegramBotsApi.registerBot(new Bot(BotConfig.USERNAME_RINNSBOT, BotConfig.TOKEN_RINNSBOT, "@ogpikek", brinn_admins));

            //Tickle
            final List<String> tickle_admins = new ArrayList<>(Arrays.asList("Donut", "xmlns", "Rinny", "grown", "BitwisePi", "nachos", "beikeila", "queencorpse"));
            telegramBotsApi.registerBot(new Bot(BotConfig.USERNAME_TICKLESBOT, BotConfig.TOKEN_TICKLESBOT, "@tickle", "-1001031081022", tickle_admins));

            telegramBotsApi.registerBot(new InlineBot(BotConfig.USERNAME_RINNLINEBOT, BotConfig.TOKEN_RINNLINEBOT));
        } catch (TelegramApiRequestException e) {
            BotLogger.error(LOGTAG, e);
        }
    }
}
