package org.telebot.updateshandlers;

import org.json.JSONObject;
import org.telebot.methods.Game;
import org.telebot.methods.Resources;
import org.telebot.methods.Spam;
import org.telegram.telegrambots.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.api.methods.groupadministration.KickChatMember;
import org.telegram.telegrambots.api.methods.groupadministration.LeaveChat;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.send.SendSticker;
import org.telegram.telegrambots.api.objects.*;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.io.File;
import java.security.InvalidParameterException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Bot extends TelegramLongPollingBot {

    private final String VERSION = "v1.8";

    //GAME//
    private static final int COMMAND_WAIT_TIME = 15 * 1000;
    public static final int GAME_WAIT_TIME = 2 * 60000;
    public long last_played_time;
    private boolean say_hi = true, paused = false;
    public boolean game_active = false;
    public String type_word, guess_word, ooo_word, taboo_word, taboo_user, scramble_word;

    //MISC//
    public final Resources resources;
    private final Game game;
    private final Spam spam;
    private String hi_message;
    public boolean MSG_SENT = true, TROLL = false;

    //STICKERS//
    private List<String> sticker_ids = new ArrayList<>(10);
    public boolean SEND_STICKERS = true;
    private boolean RECORD_STICKERS = false;

    //AUTHORISED//
    private List<String> allowed_people;
    private String allowed_name, allowed_chat_id;

    //CREDENTIALS//
    private String bot_username, token, CHAT_ID;

    public Bot(final String user, final String token, final String allowed_name, final List<String> allowed_people) {
        this.bot_username = user;
        this.token = token;
        this.allowed_name = allowed_name;
        this.allowed_people = allowed_people;
        this.resources = new Resources();
        this.game = new Game(this);
        this.spam = new Spam(this);
        this.hi_message = "Hi %u!\nWelcome to " + allowed_name;
    }

    public Bot(final String user, final String token, final String allowed_name, final String allowed_chat_id, final List<String> allowed_people) {
        this.bot_username = user;
        this.token = token;
        this.allowed_name = allowed_name;
        this.allowed_chat_id = allowed_chat_id;
        this.allowed_people = allowed_people;
        this.resources = new Resources();
        this.game = new Game(this);
        this.spam = new Spam(this);
        this.hi_message = "Hi %u!\nWelcome to " + allowed_name;
    }

    @Override
    public String getBotUsername() {
        return bot_username;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            final Message message = update.getMessage();

            //Don't respond to PMs, non-searchable groups
            if (!message.isSuperGroupMessage()) return;

            final String chat_id = message.getChatId().toString();

            //Leave disallowed groups
            if (allowed_chat_id != null) {
                if (!chat_id.equals(allowed_chat_id)) {
                    sendMsg("PM telegram.me/xmlns to get me working in this group.", chat_id);
                    leave(chat_id);
                }
            }

            final int timestamp = message.getDate();

            //Respond only to recent messages - turn off for now
//            if (System.currentTimeMillis() / 1000 - timestamp > 3) return;

            final String username = message.getFrom().getUserName();
            final int user_id = message.getFrom().getId();
            final boolean authorized = allowed_people.contains(username);

            //ANTI-SPAM FOR: 1)ALL TYPES OF MESSAGES, 2)UNAUTHORISED
            if (!authorized && spam.isSpam_HOST(username, user_id, timestamp)) {
                spam.handleSpam(message);
                return;
            }

            if (message.getNewChatMembers() != null) {
                handleNewUsers(message);
                return;
            }

            //Messages could also contain, for example, a location ( message.hasLocation() )
            if (message.hasText()) {
                String body = message.getText();
                final String name = username == null ? message.getFrom().getFirstName() : username;
                CHAT_ID = chat_id;

                ////NO LONG MESSAGES////
                if (!authorized && ((body.length() - body.replace("\n", "").length() > 25 || body.length() > 500)
                        || body.toLowerCase().contains("pollsciemo"))) {
                    final String result = kick(message.getFrom().getId(), chat_id) ?
                            (name + " was banned from the chat for posting a long message.") : "Failed to ban.";

//                    sendMsg(result, chat_id);
                    System.out.println(result);
                    deleteMsg(message.getMessageId(), CHAT_ID);
                    return;
                }

                //final String user_id = message.getFrom().getId().toString();
                System.out.println(name + ": " + body + "\n");

                ////PUBLIC COMMANDS (GAMES)////
                final boolean waiting = System.currentTimeMillis() - last_played_time < COMMAND_WAIT_TIME;

                if (body.startsWith("/") && body.endsWith(bot_username))
                    body = body.substring(0, body.lastIndexOf("@"));

                //TERMINATE GAMES
                if (game_active) {
                    if (type_word != null) {
                        if (game.isGameWord(body, Game.GameType.TYPE)) {
                            game.handleEndGame(name, chat_id);
                            return;
                        }
                    } else if (ooo_word != null) {
                        if (game.isGameWord(body, Game.GameType.ODD_ONE_OUT)) {
                            game.handleEndGame(name, chat_id);
                            return;
                        }
                    } else if (guess_word != null) {
                        if (game.isGameWord(body, Game.GameType.GUESS)) {
                            game.handleEndGame(name, chat_id);
                            return;
                        }
                    } else if (taboo_word != null) {
                        if (game.isGameWord(body, Game.GameType.TABOO)) {
                            game.handleEndGame(name, chat_id);
                            return;
                        }
                    } else {
                        if (game.isGameWord(body, Game.GameType.SCRAMBLE)) {
                            game.handleEndGame(name, chat_id);
                        }
                    }
                }

                //INITIATE GAMES
                if (!waiting && !paused && !game_active) {
                    if (game.isGameCall(body)) {
                        game.handleStartGame_HOST(body, chat_id, message);
                        return;
                    }
                }

                ////TROLL////
                if (TROLL) {
                    troll(body);
                }

                ////AUTHORIZED COMMANDS////
                if (authorized) {
                    handleAuthorisedRequests(body, chat_id);
                }
            }

            if (RECORD_STICKERS) {
                if (!message.getFrom().getUserName().equals("xmlns")) return;
                try {
                    final String id = message.getSticker().getFileId();
                    sticker_ids.add(id);
                    if (sticker_ids.size() == 10) {
                        resources.addStickerName(sticker_ids);
                        sendMsg("Sticker ids added.", message.getChatId().toString());
                    }
                    System.out.println("Sticker received. ID is: " + id + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

//            if (message.hasDocument()){
//                final String stickerID = message.getSticker().getFileId();
//                final GetFile getFile = new GetFile();
//                getFile.setFileId(stickerID);
//                try {
//                    final String filePath = getFile.getPath();
//                    final URL url = new URL(getFileUrl(getBotToken(), filePath));
//                    File file;
//                    try {
//                        file = new File(url.toURI());
//                    } catch (URISyntaxException e) {
//                        e.printStackTrace();
//                        file = new File(url.getPath());
//                    }
//                    sendImg(file, chat_id);
//                } catch ( MalformedURLException e) {
//                    e.printStackTrace();
//                }
//            }

        }
    }

    public static String getFileUrl(String botToken, String filePath) {
        if (botToken == null || botToken.isEmpty()) {
            throw new InvalidParameterException("Bot token can't be empty");
        }
        return MessageFormat.format("https://api.telegram.org/file/bot{0}/{1}", botToken, filePath);
    }

    private void troll(String body) {
        final String[] triggers = {"im", "i'm", "Im", "I'm"};
        for (String trigger : triggers) {
            if (body.contains(" " + trigger + " ") || body.startsWith(trigger + " ")) {
                final String[] words = body.split(" ");
                ArrayList<String> wordList = new ArrayList<>(Arrays.asList(words));
//                System.out.println("\n" + wordList + "\n");
                final int triggerIndex = wordList.indexOf(trigger);
                String trollName = "";
                for (int i = triggerIndex + 1; i < wordList.size(); i++) {
                    trollName += " " + wordList.get(i);
                }
                sendMsg("hey" + trollName, CHAT_ID);
                return;
            }
        }

        if (body.toLowerCase().contains(" stop ") || body.toLowerCase().startsWith("stop") || body.toLowerCase().endsWith("stop")) {
            sendMsg("u stop", CHAT_ID);
        }
    }

    private void handleAuthorisedRequests(String body, String chat_id) {
        if ("/clean".equalsIgnoreCase(body)) {
            //Get stored BotIDS and remove every one of them from the group
            JSONObject json = new Resources().getJsonFromResource("BotIDS.json");

            while (json.keys().hasNext()) {
                final int user_id = Integer.parseInt(json.keys().next());
                kick(user_id, CHAT_ID);
            }
            return;
        }

        if ("/bot".equalsIgnoreCase(body)) {
            paused = !paused;
            final String status = paused ? "The bot is now paused." : "The bot is now unpaused.";
            sendMsg(status, chat_id);
            return;
        }

        if ("/reset".equalsIgnoreCase(body)) {
            final String suffix = " game was reset.\nThe word was: ";
            if (guess_word != null) {
                sendMsg("Guess" + suffix + guess_word, chat_id);
                guess_word = null;
                game_active = false;
                game.timer.interrupt();
            } else if (type_word != null) {
                sendMsg("Type" + suffix + type_word, chat_id);
                type_word = null;
                game_active = false;
            } else if (ooo_word != null) {
                sendMsg("ooo" + suffix + ooo_word, chat_id);
                ooo_word = null;
                game_active = false;
            } else if (taboo_word != null) {
                sendMsg("Taboo" + suffix + taboo_word, chat_id);
                taboo_word = null;
                game_active = false;
                game.timer.interrupt();
            } else if (scramble_word != null) {
                sendMsg("Scramble" + suffix + scramble_word, chat_id);
                scramble_word = null;
                game_active = false;
                game.timer.interrupt();
            } else sendMsg("No games are active.", chat_id);

            return;
        }

        if ("/stickers".equalsIgnoreCase(body)) {
            SEND_STICKERS = !SEND_STICKERS;
            final String status = SEND_STICKERS ? "The bot will use stickers." : "The bot will use pictures.";
            sendMsg(status, chat_id);
            return;
        }

        if ("/hi".equalsIgnoreCase(body)) {
            say_hi = !say_hi;
            final String status = say_hi ? "The bot will greet newcomers." : "The bot will not greet newcomers.";
            sendMsg(status, chat_id);
            return;
        }

        if (body.startsWith("/hi ")) {
            hi_message = body.substring(4);
            sendMsg("Hi message was set.", chat_id);
            return;
        }

        if ((type_word == null || !body.equalsIgnoreCase(type_word))) {
            final String lower = body.toLowerCase();
            final boolean ong = lower.endsWith("ong");
            if (ong || lower.endsWith("ing")) {
                if (body.length() > 3) {
                    final int pos = body.length() - 3;
                    Character replacement = ong ? 'i' : 'o';
                    final Character character = body.charAt(pos);

                    if (character == 73 || character == 79) {
                        replacement = Character.toUpperCase(replacement);
                    }

                    final StringBuilder sb = new StringBuilder(body);
                    sb.setCharAt(pos, replacement);

                    sendMsg(sb.toString(), chat_id);
                    return;
                }
            }
        }

        if ("/id".equalsIgnoreCase(body)) {
            sendMsg(chat_id, chat_id);
            return;
        }

        if ("/version".equalsIgnoreCase(body)) {
            sendMsg(VERSION, chat_id);
            return;
        }

        if ("/troll".equalsIgnoreCase(body)) {
            TROLL = !TROLL;
            final String msg = TROLL ? "finnnnnaa troll" : "The bot will not troll.";
            sendMsg(msg, chat_id);
            return;
        }

        if ("/json".equalsIgnoreCase(body)) {
            sendMsg(resources.getJsonFromResource("StickerNames.json").toString(), chat_id);
        }
    }

    private void handleNewUsers(Message message) {
        final List<User> newUsers = message.getNewChatMembers();

        for (final User newUser : newUsers) {
            final String newUsername = newUser.getUserName();
            final String newName = newUsername == null ? newUser.getFirstName() : newUsername;
            final String chat_id = message.getChatId().toString();

            //DISALLOW OTHER BOTS
            if (newName.toLowerCase().endsWith("bot")) {
                new Resources().addToJsonFile(newUser.getId().toString(), newName, "BotIDS.json");
                kick(newUser.getId(), chat_id);
                System.out.println("Banned " + newUsername);
            } else if (newName.toLowerCase().contains("Aceti")) {
//                kick(newUser.getId(), chat_id);
//                System.out.println("Banned " + newUsername);
            } else if (say_hi) {
                sendMsg(hi_message.replace("%u", newName), chat_id);
            }
        }
    }

    private void handleBlockedUser() {
        MSG_SENT = false;
        final String msg = "You must start chatting with me to receive the word.\nPress the button below and try again.";

        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButton.setText("Start me");
        inlineKeyboardButton.setUrl("https://telegram.me/" + bot_username);

        List<InlineKeyboardButton> button = new ArrayList<>(1);
        button.add(inlineKeyboardButton);

        List<List<InlineKeyboardButton>> buttonRow = new ArrayList<>(1);
        buttonRow.add(button);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(buttonRow);

        sendMsgWithBoard(msg, inlineKeyboardMarkup);
    }

    ////ACTIONS////
    public void sendMsg(String msg, String chatId) {
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.setText(msg);
        sendMessageRequest.setChatId(chatId);
        try {
            sendMessage(sendMessageRequest);
        } catch (TelegramApiException e) {
            if ("Bot was blocked by the user".equals(e.getMessage()) || "PEER_ID_INVALID".equals(e.getMessage())) {
                handleBlockedUser();
            } else e.printStackTrace();
        }
    }

    private void sendMsgWithBoard(String msg, InlineKeyboardMarkup buttons) {
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.setText(msg);
        sendMessageRequest.setChatId(CHAT_ID);
        sendMessageRequest.setReplyMarkup(buttons);

        try {
            sendMessage(sendMessageRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public boolean kick(int user_id, String chat_id) {
        KickChatMember kickMemberRequest = new KickChatMember();
        kickMemberRequest.setUserId(user_id);
        kickMemberRequest.setChatId(chat_id);
        try {
            return kickMember(kickMemberRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void sendImg(File image, String chatId) {
        SendPhoto sendPhotoRequest = new SendPhoto();
        sendPhotoRequest.setNewPhoto(image);
        sendPhotoRequest.setChatId(chatId);
        try {
            sendPhoto(sendPhotoRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void deleteMsg(int message_id, String chat_id) {
        DeleteMessage deleteMessageRequest = new DeleteMessage();
        deleteMessageRequest.setChatId(chat_id);
        deleteMessageRequest.setMessageId(message_id);
        try {
            deleteMessage(deleteMessageRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendAct(String action) {
        // Deprecated
    }

    public void sendSticker(String file_id, String chat_id) {
        SendSticker sendStickerRequest = new SendSticker();
        sendStickerRequest.setSticker(file_id);
        sendStickerRequest.setChatId(chat_id);
        try {
            sendSticker(sendStickerRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void leave(String chat_id) {
        LeaveChat leaveChatRequest = new LeaveChat();
        leaveChatRequest.setChatId(chat_id);
        try {
            leaveChat(leaveChatRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}