package org.telebot.updateshandlers;

import org.telebot.methods.Util;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.util.ArrayList;
import java.util.List;

public class InlineBot extends TelegramLongPollingBot {

    //CREDENTIALS//
    private String username, token;

    public InlineBot(final String user, final String token) {
        this.username = user;
        this.token = token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasInlineQuery()) {
            //Respond only to recent messages
            try {
                InlineQuery inlineQuery = update.getInlineQuery();
                final String query = inlineQuery.getQuery();
                if (!query.isEmpty()) answerInlineQuery(getInline(inlineQuery));
            } catch (TelegramApiException e) {
                System.err.print(e.toString());
            }
        }
    }

    @Override
    public String getBotToken() {
        // TODO Auto-generated method stub
        return token;
    }

    private AnswerInlineQuery getInline(InlineQuery inlineQuery) {
        List<InlineQueryResult> results = new ArrayList<>();
        final String query = inlineQuery.getQuery();
        final String query_result = commands_RINN(query);
        final String desc = query_result.equals("No such command found.") ? query_result : "Command found, click here to send.";

        InputTextMessageContent messageContent = new InputTextMessageContent();
        messageContent.disableWebPagePreview();
        messageContent.setMessageText(query_result);

        InlineQueryResultArticle article = new InlineQueryResultArticle();
        article.setInputMessageContent(messageContent);
        article.setId("0");
        article.setTitle(query);
        article.setDescription(desc);

        results.add(article);

        AnswerInlineQuery answerInlineQuery = new AnswerInlineQuery();
        answerInlineQuery.setInlineQueryId(inlineQuery.getId());
        answerInlineQuery.setResults(results);

        return answerInlineQuery;
    }

    private String commands_RINN(String msg) {
        if (msg.startsWith("!c ") && msg.length() >= 4)
            msg = Util.getBlueLetters(msg.substring(3).toLowerCase());
        else if (msg.startsWith("!ud ") && msg.length() >= 5)
            msg = Util.getDefinition(msg.substring(4).toLowerCase());
        else if (msg.equalsIgnoreCase("!gun"))
            msg = " ,-._______,========,   \n[  |)________)####(  (_\n  \\ =====.-.___, - - \"   __\\\n    \"-._,_,_[HD] __\\##/\n              \\  (    )) )#O#(    \n                \\ \\___/,.###\\  \n                  `===\" \\###\\  \n                              )#O#|\n                              )###\" \n                             `- - \" \"";
        else if (msg.equalsIgnoreCase("!rip"))
            msg = "\u2229 \u2020   \u03a0 \u03a0    \u2020 \u2229\u03a0   \u03a0 \u2020\n\u256d\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u256e   \u2020 \u03a0\n\u2503                    \u2503     \u2229\u2229 \u2020\n\u2503      R.I.P      \u2503   \n\u2503                    \u2503\n\u2503                    \u2503\n\u2503                    \u2503\n\u2503                    \u2503";
        else if (msg.equalsIgnoreCase("!shrug")) msg = "\u00af\\_(\u30c4)_/\u00af";
        else if (msg.equalsIgnoreCase("!fab"))
            msg = "\u2282_\u30fd\n\u3000 \uff3c\uff3c \uff3f\n\u3000\u3000 \uff3c(\u3000\u2022_\u2022) F\n\u3000\u3000\u3000 <\u3000\u2312\u30fd A\n\u3000\u3000\u3000/ \u3000 \u3078\uff3c B\n\u3000\u3000 /\u3000\u3000/\u3000\uff3c\uff3c U\n\u3000\u3000 \uff9a\u3000\u30ce\u3000\u3000 \u30fd_\u3064 L\n\u3000\u3000/\u3000/                   O\n\u3000 /\u3000/|                   U\n\u3000(\u3000(\u30fd               S\n\u3000|\u3000|\u3001\uff3c\n\u3000| \u4e3f \uff3c \u2312)\n\u3000| |\u3000\u3000) /\n`\u30ce )\u3000   L\uff89\n(_\uff0f";
        else if (msg.equalsIgnoreCase("!cat")) msg = "　　／￣￣ヽ￣￣＼\n 　∠　　レ  |　 ⌒   \\\n　　＼＿＿ノ丶　　)|\n　　 (_(＿＿_ノ⊂ニノ";
        else if (msg.equalsIgnoreCase("!cat2"))
            msg = "﻿　　　　    　＿＿＿\n　　　　　／ フ　   フ\n　　　　　| 　_　  _ |\n　 　　　／`ミ＿x_彡\n　　  　 /　　　 　 |\n　　　 /　 ヽ　　 ﾉ\n　  　 │　　|　|　|\n　／￣|　　 |　|　|\n　| (￣ヽ＿_ヽ_)__)\n　＼二つ\n";
        else if (msg.equalsIgnoreCase("!r2d2"))
            msg = "★    .-ー -.\n      /_ |●|＿\\\n   _|▁[]o▁LI|_|_\n/░ |  ≡≡≡  |░\\\n|▒|] ≡≡≡  [|▒|  ★\n｜|| ▁▂▃  ||  |  |\n｜| \\▒/▒\\▒/ |｜\n/＿|   /▂\\   |＿\\";
        else if (msg.equalsIgnoreCase("!bunny"))
            msg = "﻿    |  \\               /  |\n    |  \\  \\        /  /  |\n    |   \\    |    |   /   |\n     \\   |   |    |  |   /\n       \\ |   | _ |  | /\n      .  '             '  .\n      |    o       o   |     \n     /      = Y =    \\\n      ` ' -  . ^ .  - ' `\n          _|        |_\n    / `                  ` \\\n    |                        |\n    |       (       )       |\n   / \\       \\  /       / \\\n   |        ' ._)_. '       |\n    \\                       /\n      \\     '.___.'     /\n  .--'      \\---/    '--.\n  '------- '      '-------`";
        else if (msg.equalsIgnoreCase("!rekt2"))
            msg = "┏━━━━━┓\n┃   ┏━┓   ┃\n┃   ┃   ┃   ┃\n┃   ┗━┛    ┃\n┃   ┏┓   ┏┛\n┃   ┃ ┃   ┗┓\n┃   ┃  ┗┓  ┗┓\n┗━┛　 ┗━━┛\n      ███╗\n      █╔═╝\n      ███╗\n      █╔═╝\n      ███╗\n      ╚══╝\n▒▒▒▒▒▒▒▒\n▒██▒▒██▒\n▒██▒██▒▒\n▒████▒▒▒\n▒██▒██▒▒\n▒██▒▒██▒\n▒▒▒▒▒▒▒▒\n\n██████████\n██████████\n       ████\n       ████\n       ████\n       ████\n       ████\n       ████";
        else if (msg.equalsIgnoreCase("!footprints"))
            msg = "﻿     ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 \n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 \n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 \n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜\n\n　　　 ｡｡\n　　　ﾟ●゜\n\n　 ｡｡\n　ﾟ●゜";
        else if (msg.equalsIgnoreCase("!pig"))
            msg = "﻿    ┏━╮╭━┓\n    ┃┏┗┛┓┃\n    ╰┓▋▋┏╯\n  ╭━┻╮╲┗━━━━━╮╭╮\n  ┃▎▎┃╲╲╲╲╲╲╲┣━╯\n  ╰━┳┻▅╯╲╲╲╲╲┃\n       ╰━┳┓┏━┳┓┏╯\n";
        else if (msg.equalsIgnoreCase("!christmas"))
            msg = "❄ Merry*❄ 。 • ˚ ˚ ˛ ˚ ˛ • ❄\n•。★❄ Christmas❄ 。* 。❄\n° 。 ° ˛˚˛     _Π_____*。*˚\n˚ ˛❄ •˛•˚ */_______/＼。˚\n˚. •˛• ❄˚ ｜ 田田 ｜｜ ˚\n⌒⌒⌒⌒⌒⌒⌒⌒⌒⌒⌒";
        else if (msg.equalsIgnoreCase("!flipstable"))
            msg = "　　　　　　／匚＼\n　   ∧_∧　匚　  　 |\n　 (∩ > <)∩ |　   匚/ \n　 (ノ　 )ノ 匚＿_／\n　⊂＿)_)　彡";
        else if (msg.equalsIgnoreCase("!haha"))
            msg = "﻿    　     ∧＿∧\n     o ⌒(  ´ ∀ `)つ〃\n      'と＿)_つノ\n╔╗╔╦══╦╗╔╦══╗\n║╚╝║╔╗║╚╝║╔╗║\n║╔╗║╔╗║╔╗║╔╗║\n╚╝╚╩╝╚╩╝╚╩╝╚╝";
        else if (msg.equalsIgnoreCase("!nude"))
            msg = "﻿   ( . )( . )\n    \\      /\n    /   .  \\\n   |    Y   |\n    \\   |   /";
        else if (msg.equalsIgnoreCase("!goku"))
            msg = "　　　 |＼\n　／￣＼　ヽ\n／　　　ヽ |_\n￣＼　　　　 ＼\n／￣　/|/| /ﾚ､ ヽ＿\n￣＼ Yヘ |/ヘ幺　∠\n　＜_|(･    ･) 6) ／\n　       (ﾞ _　    ／＜\n　　　＞､ーイ￣￣\n　    ／厂ヽ／￣/＼\n　    ( ｜　   (亀)(　)\n　    \\(ﾐ)ﾆ只ニ(ﾐ_／\n　　    /〈/L〉 ヽ\n　　  ｜　ヽ　　|\n　     〈＿／ ＼＿〉\n　     ／　)　   (　＼\n　     ￣￣   　  ￣￣";
        else if (msg.equalsIgnoreCase("!girl"))
            msg = "　 へ／⌒⌒ヽ\n　(　ﾚｲ从ﾚ从) )\n　 )ﾉc､ﾟヮﾟ人(\n　　   [ つ⊂|\n　　 くﾉｪｪ|｣\t\n　　　ＵＵ";
        else if (msg.equalsIgnoreCase("!kick"))
            msg = "﻿   ∧_∧   \n（´･ω･)          ∧_∧ \n⊂( ⊆ レつ☆))'Д' )\n　ヽ　／  ⊂ ⊂~ﾉ\n  　と丿   ⊂と_ノ”";
        else if (msg.equalsIgnoreCase("!imtheshit"))
            msg = "﻿   _    ∧ _∧\n /   \\ (   > < )\n|　  |( つ　つ    ＿\nヽ.   (　⌒)  ) (三(@\nに二二二U J\n   ）　 　r' I'm the\n   └ーー-┘   shit";
        else if (msg.equalsIgnoreCase("!lenny"))
            msg = "( \u0361\u00b0 \u035c\u0296 \u0361\u00b0)";
        else if (msg.equalsIgnoreCase("!bread"))
            msg = "\u30fe( \uff61\u003e\ufe4f\u003c\uff61 )\uff89\uff9e \u2727\u002a\u3002";
        else msg = "No such command found.";
        return msg;
    }

}