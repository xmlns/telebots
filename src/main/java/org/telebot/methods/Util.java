package org.telebot.methods;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;

/**
 * Created by Amogh K on 8/8/16.
 */

public class Util {

    public static String getDefinition(final String word) {
        try {
            final URL url = new URL("http://api.urbandictionary.com/v0/define?term=" + URLEncoder.encode(word, "UTF-8"));
            final URLConnection connection = url.openConnection();

            try (final BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                StringBuilder output = new StringBuilder();
                String inputLine;

                while ((inputLine = input.readLine()) != null) {
                    output.append(inputLine).append("\n");
                }

                input.close();

                final JSONObject jsonData = new JSONObject(output.toString());
                final String existence = jsonData.getString("result_type");

                if (!existence.equals("exact")) {
                    return word + "\n\nNo definition found\n\n";
                } else {
                    final JSONArray array = jsonData.getJSONArray("list");
                    return word + "\n\n" + array.getJSONObject(0).getString("definition").trim();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error:\n\n" + e.toString();
        }
    }

    public static String getBlueLetters(final String word) {
        return word.replace("a", "‌🇦").replace("b", "‌🇧").replace("c", "‌🇨").replace("d", "‌🇩").replace("e", "‌🇪").replace("f", "‌🇫").replace("g", "‌🇬").replace("h", "‌🇭").replace("i", "‌🇮").replace("j", "‌🇯").replace("k", "‌🇰").replace("l", "‌🇱").replace("m", "‌🇲").replace("n", "‌🇳").replace("o", "‌🇴").replace("p", "‌🇵").replace("q", "‌🇶").replace("r", "‌🇷").replace("s", "‌🇸").replace("t", "‌🇹").replace("u", "‌🇺").replace("v", "‌🇻").replace("w", "‌🇼").replace("x", "‌🇽").replace("y", "‌🇾").replace("z", "‌🇿").replace("?", "‌❔").replace("!", "‌❕");
    }

    public static String[] scramble(final String[] words) {
        final int len = words.length;
        final String[] scrambled = new String[len];

        for (int i = 0; i < len; i++) {
            scrambled[i] = scramble(words[i]);
        }

        return scrambled;
    }

    public static String scramble(final String word) {
        final char[] letters = word.toCharArray();
        final int len = letters.length;
        final Random random = new Random();
        int index, newIndex;
        char temp;

        for (int i = 0; i < len; i++) {
            index = random.nextInt(len);
            newIndex = random.nextInt(len);

            temp = letters[index];

            letters[index] = letters[newIndex];
            letters[newIndex] = temp;
        }

        return new String(letters);
    }


}
