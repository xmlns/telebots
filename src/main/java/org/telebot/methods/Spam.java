package org.telebot.methods;

import org.telebot.updateshandlers.Bot;
import org.telegram.telegrambots.api.objects.Message;

import java.util.*;

/**
 * Created by Amogh K on 7/19/2016.
 */

public class Spam {
    private final Bot bot;
    private final Map<Integer, List<Integer>> message_timestamps = new HashMap<>();

    public Spam(final Bot bot){
        this.bot = bot;
    }

    public boolean isSpam_HOST(final String username, final int user_id, final int timestamp) {
        final boolean isNehal = "petite".equals(username); // don't ban Nehal
        if (isNehal) {
            return false;
        }

        final int maxTime = 7;
        final int maxAmount = 5;
        List<Integer> stamps = message_timestamps.get(user_id);

        if (stamps == null) {
            stamps = new ArrayList<>(5);
            message_timestamps.put(user_id, stamps);
        }

        for (final Iterator<Integer> iter = stamps.iterator(); iter.hasNext(); ) {
            if (System.currentTimeMillis() / 1000 - iter.next() > maxTime) iter.remove();
        }

        stamps.add(timestamp);
        return stamps.size() >= maxAmount;
    }

    public void handleSpam(final Message message) {

        final String chat_id = message.getChatId().toString();
        final int user_id = message.getFrom().getId();
        final String result = bot.kick(message.getFrom().getId(), message.getChatId().toString()) ?
                (message.getFrom().getUserName() + " was banned from the chat for spamming.") : "Failed to ban.";

        bot.sendMsg(result, chat_id);
        message_timestamps.remove(user_id);
    }
}
