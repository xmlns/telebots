package org.telebot.methods;

import org.json.JSONObject;
import org.telebot.updateshandlers.Bot;
import org.telegram.telegrambots.api.objects.Message;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Amogh K on 7/19/2016.
 */

public class Game {
    public enum GameType {
        TYPE,
        ODD_ONE_OUT,
        GUESS,
        TABOO,
        SCRAMBLE,
    }

    private final Bot bot;
    public Thread timer;

    public Game(Bot bot) {
        this.bot = bot;
    }

    public boolean isGameCall(String body) {
        return (body.equalsIgnoreCase("/type") || body.equalsIgnoreCase("/ooo") || body.equalsIgnoreCase("/guess")) || body.equalsIgnoreCase("/taboo") || body.equalsIgnoreCase("/scramble");
    }

    public void handleStartGame_HOST(final String body, final String chat_id, final Message message) {
        if ("/type".equalsIgnoreCase(body)) {

            final String word = Word_HOST.getRandomWord();
            bot.sendMsg("Type the following:\n\n" + word, chat_id);

            bot.last_played_time = System.currentTimeMillis();
            bot.type_word = word;
            bot.game_active = true;

        } else if ("/guess".equalsIgnoreCase(body)) {

            if (bot.SEND_STICKERS) {

                final JSONObject list = bot.resources.getJsonFromResource("StickerNames.json");
                final int len = list.length();
                final List<String> keys = new ArrayList<>(list.keySet());

                final String key = keys.get(new Random().nextInt(len));
                final String sticker_id = list.getString(key);
                final String word = key.substring(0, key.indexOf("."));

                bot.sendSticker(sticker_id, chat_id);
                bot.last_played_time = System.currentTimeMillis();
                bot.guess_word = word;
                bot.game_active = true;

                gameTimer(word, chat_id);

            } else {

                final String path = "guess/";
                final List<String> files = bot.resources.getResourceFiles(path);
                final String pic = files.get(new Random().nextInt(files.size()));
                final File image = bot.resources.getFileFromResource(path + pic);
                final String word = pic.substring(0, pic.indexOf("."));

                bot.sendImg(image, chat_id);
                bot.last_played_time = System.currentTimeMillis();
                bot.guess_word = word;
                bot.game_active = true;

                gameTimer(word, chat_id);

            }

        } else if ("/ooo".equalsIgnoreCase(body)) {

            final OddOneOut ooo = new OddOneOut();

            bot.sendMsg(ooo.getRandomWords(), chat_id);

            bot.last_played_time = System.currentTimeMillis();
            bot.ooo_word = ooo.getOddWord();
            bot.game_active = true;

        } else if ("/taboo".equalsIgnoreCase(body)) {

            final String username = message.getFrom().getUserName();
            final String name = username == null ? message.getFrom().getFirstName() : username;

            if (name.equals(bot.taboo_user)) return; //Can't get the word twice in a row

            final String id = message.getFrom().getId().toString();
            final String word = Word_HOST.getRandomWord();

            bot.sendMsg("The word is: " + word, id);

            if (!bot.MSG_SENT) {
                bot.MSG_SENT = true;
                return;
            }

            bot.sendMsg("Word sent to " + name + ".", chat_id);
            bot.last_played_time = System.currentTimeMillis();
            bot.taboo_word = word;
            bot.taboo_user = name;
            bot.game_active = true;

            gameTimer(word, chat_id);

        } else if ("/scramble".equalsIgnoreCase(body)) {

            final JSONObject list = bot.resources.getJsonFromResource("Scrambled.json");
            final int len = list.length();
            final List<String> keys = new ArrayList<>(list.keySet());
            final String unscrambled = keys.get(new Random().nextInt(len)), scrambled = list.getString(unscrambled);

            bot.sendMsg("Unscramble the following:\n\n" + scrambled, chat_id);
            bot.last_played_time = System.currentTimeMillis();
            bot.scramble_word = unscrambled;
            bot.game_active = true;

            gameTimer(unscrambled, chat_id);

        }
    }

    public void handleEndGame(String username, String chat_id) {
        final long time = System.currentTimeMillis();

        if (bot.type_word != null) {
            final long win_time = time - bot.last_played_time;
            final long rate = win_time / bot.type_word.length();
            final String msg = username + " has won in " + win_time
                    + " milliseconds with a rate of " + rate + " ms/char!";

            bot.sendMsg(msg, chat_id);
            bot.last_played_time = time;
            bot.type_word = null;
            bot.game_active = false;
            return;
        }

        if (bot.guess_word != null) {
            bot.sendMsg(username + " has guessed the pic correctly!", chat_id);
            bot.last_played_time = time;
            bot.guess_word = null;
            bot.game_active = false;
            timer.interrupt();
            return;
        }

        if (bot.ooo_word != null) {
            final long win_time = time - bot.last_played_time;
            final String msg = username + " has won in " + win_time + " milliseconds.";

            bot.sendMsg(msg, chat_id);
            bot.last_played_time = time;
            bot.ooo_word = null;
            bot.game_active = false;
            return;
        }

        if (bot.taboo_word != null) {
            final String suffix = "\nThe word was: " + bot.taboo_word;

            if (username.equalsIgnoreCase(bot.taboo_user)) {
                final String msg = username + " is a CHEATER!" + suffix;
                bot.sendMsg(msg, chat_id);
            } else {
                final String msg = username + " has won in taboo!" + suffix;
                bot.sendMsg(msg, chat_id);
            }

            bot.last_played_time = time;
            bot.taboo_word = null;
            bot.game_active = false;
            timer.interrupt();
            return;
        }

        if (bot.scramble_word != null) {
            final String msg = username + " has won in scramble!\nThe word was: " + bot.scramble_word;
            bot.sendMsg(msg, chat_id);

            bot.last_played_time = time;
            bot.scramble_word = null;
            bot.game_active = false;
            timer.interrupt();
        }
    }

    public boolean isGameWord(final String body, final GameType game) {
        switch (game) {
            case TYPE:
                return body.equalsIgnoreCase(bot.type_word);
            case ODD_ONE_OUT:
                return body.equalsIgnoreCase(bot.ooo_word);
            case GUESS:
                return body.replaceAll("[^A-Za-z0-9]", "").toLowerCase().contains(bot.guess_word);
            case TABOO:
                return body.toLowerCase().contains(bot.taboo_word);
            case SCRAMBLE:
                return body.equalsIgnoreCase(bot.scramble_word);
            default:
                return false;
        }
    }

    private void gameTimer(String word, String chat_id) {
        this.timer = new Thread(() -> {
            try {
                Thread.sleep(Bot.GAME_WAIT_TIME);

                bot.taboo_word = null;
                bot.guess_word = null;
                bot.game_active = false;
                bot.sendMsg("Game timed out.\nThe word was: " + word, chat_id);
                bot.last_played_time = System.currentTimeMillis();

            } catch (InterruptedException e) {
//                e.printStackTrace(); //Not necessary tbh?
                System.err.println("Timer interrupted. The word was: " + word + "\n");
            }
        });

        timer.start();
    }

}