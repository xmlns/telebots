package org.telebot.methods;

import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by Amogh K on 7/19/2016.
 */
public class Resources {

    List<String> getResourceFiles(String path) {
        List<String> filenames = new ArrayList<>(420);

        try (InputStream in = getResourceAsStream(URLDecoder.decode(path, "UTF-8"));
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {

            String resource;
            while ((resource = br.readLine()) != null) {
                filenames.add(resource);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filenames;
    }

    private InputStream getResourceAsStream(String resource) {
        final InputStream in = getContextClassLoader().getResourceAsStream(resource);
        return in == null ? getClass().getResourceAsStream(resource) : in;
    }

    private ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public JSONObject getJsonFromResource(String fileName) {

        StringBuilder buff = new StringBuilder("");

        try (Scanner sc = new Scanner(getFileFromResource("JSON/" + fileName))) {

            while (sc.hasNextLine()) buff.append(sc.nextLine());

            sc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject(buff.toString());

    }

    public File getFileFromResource(String path) {

        final URL url = getContextClassLoader().getResource(path);

        try {
            return new File(URLDecoder.decode(url.getPath(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

    }

    public void hashMapToJson(final HashMap keyValueMap, final String fileName) {

        final Resources res = new Resources();
        final JSONObject list = res.getJsonFromResource(fileName);
        final Set set = keyValueMap.entrySet();
        final Iterator it = set.iterator();
        String key, value;

        while (it.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) it.next();
            key = mapEntry.getKey().toString();
            value = mapEntry.getValue().toString();

            System.out.println(key + " " + value);

            list.put(key, value);
        }

        try (PrintWriter pw = new PrintWriter(res.getFileFromResource("JSON/" + fileName))) {
            pw.print(list.toString());
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addStickerName(List<String> ids) {

        try {
            final JSONObject list = getJsonFromResource("StickerNames.json");
            assert list != null;
            final String[] files = new File("E:/Download files/BotGuess/").list();

            for (int i = 0; i < 10; i++) {
                list.put(files[i], ids.get(i));
            }

            try (PrintWriter pw = new PrintWriter(getFileFromResource("JSON/StickerNames.json"))) {
                pw.print(list.toString());
                pw.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addToJsonFile(String key, String value, String fileName) {
        try {
            final JSONObject list = getJsonFromResource(fileName);

            list.putOnce(key, value);
            try (PrintWriter pw = new PrintWriter(getFileFromResource("JSON/" + fileName))) {
                pw.print(list.toString());
                pw.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
