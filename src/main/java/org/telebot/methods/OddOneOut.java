package org.telebot.methods;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

class OddOneOut {
    final private static String[][] arrays = {
            {"acceptance", "admiration", "adoration", "affection", "afraid", "agitation", "agreeable", "aggressive", "aggravation", "agony", "alarm", "alienation", "amazement", "amusement", "anger", "angry", "anguish", "annoyance", "anticipation", "anxiety", "apprehension", "assertive", "assured", "astonishment", "attachment", "attraction", "awe", "beleaguered", "bewitched", "bitterness", "bliss", "blue", "boredom", "calculating", "calm", "capricious", "caring", "cautious", "charmed", "cheerful", "closeness", "compassion", "complacent", "compliant", "composed", "contempt", "conceited", "concerned", "content", "contentment", "crabby", "crazed", "crazy", "cross", "cruel", "defeated", "defiance", "delighted", "dependence", "depressed", "desire", "disappointment", "disapproval", "discontent", "disenchanted", "disgust", "disillusioned", "dislike", "dismay", "displeasure", "dissatisfied", "distraction", "distress", "disturbed", "dread", "eager", "earnest", "easy-going", "ecstasy", "ecstatic", "elation", "embarrassment", "emotion", "emotional", "enamored", "enchanted", "enjoyment", "enraged", "enraptured", "enthralled", "enthusiasm", "envious", "envy", "equanimity", "euphoria", "exasperation", "excited", "exhausted", "extroverted", "exuberant"},
            {"acorn", "sprouts", "almond", "anchovy", "anise", "appetizer", "appetite", "apple", "apricot", "artichoke", "asparagus", "avocado", "bacon", "bagel", "bake", "banana", "barbecue", "barley", "basil", "batter", "beancurd", "beans", "beef", "beet", "berry", "biscuit", "blackberry", "bland", "blueberry", "boil", "bowl", "boysenberry", "bran", "bread", "breadfruit", "breakfast", "brisket", "broccoli", "broil", "brownie", "brunch", "buckwheat", "buns", "burrito", "butter", "cake", "calorie", "candy", "cantaloupe", "capers", "caramel", "carbohydrate", "carrot", "cashew", "cassava", "casserole", "cater", "cauliflower", "caviar", "cayenne pepper", "celery", "cereal", "chard", "cheddar", "cheese", "cheesecake", "chef", "cherry", "chew", "chicken", "chick peas", "chili", "chips", "chives", "chocolate", "chopsticks", "chow", "chutney", "cilantro", "cinnamon", "citron", "citrus", "clam", "cloves", "coconut", "cod", "coffee", "coleslaw", "collard greens", "comestibles", "cook", "cookbook", "cookie", "corn", "cornflakes", "cornmeal", "cottage cheese", "crab", "crackers", "cranberry", "cream", "cream cheese", "crepe", "crisp", "crunch", "crust", "cucumber", "cuisine", "cupboard", "cupcake", "curds", "currants", "curry", "custard"},
            {"academy", "admiral", "advance", "aircraft", "AirForce", "ally", "ammo", "ammunition", "armistice", "armor", "armory", "arms", "army", "arrow", "arsenal", "artillery", "assault", "attack", "attention", "ballistic", "barracks", "base", "battalion", "battle", "battlefield", "battery", "battleship", "bayonet", "besiege", "billet", "bivouac", "bomb", "bombard", "bombardment", "brig", "brigade", "bullet", "cadet", "camouflage", "camp", "cannon", "cannon ball", "canteen", "captain", "capture", "carrier", "casualty", "catapult", "cavalry", "chaplain", "coast guard", "colonel", "combat", "command", "commander", "commanding officer", "commission", "company", "conflict", "conquest", "conscription", "convoy", "corporal", "corps", "covert", "crew", "decode", "defeat", "defend", "defense", "destroyer", "detonate", "division", "dogtags", "encampment", "encode", "enemy", "engage", "evacuate", "explosive", "radar", "rank", "reconnoiter", "recruit", "regiment", "rescue", "reserves", "retreat", "ribbon", "rifle", "tactical", "tactics", "tank", "taskforce", "theater", "tomahawk", "torpedo", "troops", "truce"},
            {"afford", "ATM", "auction", "bailout", "balance", "bank", "bankrupt", "bankruptcy", "bargain", "bill", "bonds", "borrow", "bought", "budget", "business", "buy", "capital", "cash", "cent", "change", "cheap", "check", "collateral", "commodity", "coupon", "credit", "currency", "debt", "defecit", "deposit", "depression", "dime", "discount", "diversify", "dividend", "dollar", "donate", "donation", "donor", "earn", "earnings", "economy", "economics", "economist", "equity", "estate", "Euro", "exchange", "expense", "expensive", "finance", "financier", "fund", "income", "interest", "invest", "investment", "investor", "lend", "lender", "loan", "loss", "margin", "market", "money", "mortgage", "nickel", "note"},
            {"amber", "amethyst", "apricot", "aqua", "aquamarine", "auburn", "azure", "beige", "black", "blue", "bronze", "brown", "cerulean", "charcoal", "chocolate", "color", "coral", "crimson", "cyan", "dark", "denim", "ebony", "ecru", "eggplant", "emerald", "forest green", "fuchsia", "gold", "goldenrod", "gray", "green", "grey", "hue", "indigo", "ivory", "jade", "jet", "khaki", "lavender", "lemon", "light", "lilac", "lime", "magenta", "mahogany", "maroon", "mauve", "mustard", "ocher", "olive", "orange", "pastel", "peach", "pewter", "pink", "primary", "puce", "pumpkin", "purple", "rainbow", "red", "rose", "ruby"}
    };
    private String odd_word;

    private static int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
        int random = start + rnd.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }

    String getRandomWords() {
        final Random random = new Random();
        final int categ_array = random.nextInt(5);
        final int categ_element = random.nextInt(arrays[categ_array].length - 1);

        final int odd_array = getRandomWithExclusion(random, 0, 4, categ_array);
        final int odd_element = random.nextInt(arrays[odd_array].length - 1);

        odd_word = arrays[odd_array][odd_element];

        final String first = arrays[categ_array][categ_element];
        final String second = arrays[categ_array][random.nextInt(arrays[categ_array].length)];

        final String[] words = {first, second, odd_word};
        Collections.shuffle(Arrays.asList(words));

        return ("Find the Odd One Out:\n\n" + words[0] + ", " + words[1] + ", " + words[2]);
    }

    String getOddWord() {
        return odd_word;
    }
}