package org.telebot.methods;

import java.util.HashMap;

/**
 * Created by rinn on 29/8/16.
 */

public class Scramble {

    public static void main(String[] args) {
        final String[] words = Word_HOST.getArray();
        new Scramble().makeHashMap(words);
    }

    private void makeHashMap(final String[] words) {
        final HashMap<String, String> hashMap = new HashMap<>();
        final String[] scrambled = Util.scramble(words);

        for (int i = 0; i < words.length; i++) {
            if (!words[i].equals(scrambled[i])) {
                hashMap.put(words[i], scrambled[i]);
            }
        }

        new Resources().hashMapToJson(hashMap, "Scrambled.json");
    }
}
